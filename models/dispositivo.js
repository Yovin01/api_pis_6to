'use strict';
const { UUIDV4 } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    const dispositivo = sequelize.define('dispositivo', {
        latitud: { type: DataTypes.STRING(20), allowNull: false },
        longitud: { type: DataTypes.STRING(20), allowNull: false },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
    }, {
        freezeTableName: true
    });

    dispositivo.associate = function (models){
        dispositivo.hasMany(models.medicion, {foreignKey: 'id_dispositivo', as: 'medicion'});
    }
    return dispositivo;
};
