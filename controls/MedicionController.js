'use strict';

//const cron = require('cron');
const { Console } = require('console');
const dispositivo = require('../models/dispositivo');
var models = require('../models/');
var medicion = models.medicion;
class MedicionController {

    async obtenerDFeachas(req, res) {
        var date1 = new Date(req.params.f0);
        var date2 = new Date(req.params.fi);
        const diferenciaEnMilisegundos = Math.abs(date2 - date1);
        const diferenciaEnDias = diferenciaEnMilisegundos / (1000 * 60 * 60 * 24);
        console.log(diferenciaEnDias + " -- "+ diferenciaEnMilisegundos);
        // Comparar si la diferencia es mayor de 60 días
        if (diferenciaEnDias > 60) {
            res.status(500);
            res.json({ msg: 'Las fechas no son validas, maximo 60 dias', code: 500 });
        } else {
            date2.setDate(date2.getDate() + 1);
            if (diferenciaEnDias < 0) {var datex = date1;
                date1 = date2;
                date2 = datex;}
        
            var mediciones = await medicion.findAll({
                where: {createdAt: { [models.Sequelize.Op.between]: [date1, date2]}},//'DATE(medicion.createdAt) BETWEEN ' + date1 + ' AND ' + date2 +';'// Ejemplo: '2024-01-20' AND '2024-01-26';'
                // Aquí puedes incluir otras configuraciones según tus necesidades 
                // tre entre fechas [models.Sequelize.Op.between]: [date1, date2]
            });
            res.status(200);
            res.json({msg:'OK!',code:200,info:mediciones});
        }

    }
    async obtenerDia(req, res) {
        var date1 = new Date(req.params.dia);
        const query = `SELECT * FROM  medicion WHERE DATE(medicion.createdAt) = '`+req.params.dia+`';`;
        var lista = await models.sequelize.query(query);
            res.status(200);
            res.json({msg:'OK!',code:200,info:lista});
       
    }

}

module.exports = MedicionController;

/*
-- Insertar datos en la tabla 'ubicacion'
INSERT INTO dispositivo (latitud, longitud, external_id, createdAt, updatedAt) VALUES
  (CONCAT(ROUND(RAND() * 90, 6), '°'), CONCAT(ROUND(RAND() * 180, 6), '°'), UUID(), NOW(), NOW()),
  (CONCAT(ROUND(RAND() * 90, 6), '°'), CONCAT(ROUND(RAND() * 180, 6), '°'), UUID(), NOW(), NOW()),
  -- Repite la estructura para agregar más filas

-- Insertar datos en la tabla 'medicion'
INSERT INTO medicion (indice_uv, external_id, createdAt, updatedAt, id_dispositivo) VALUES
  (ROUND(RAND() * 9, 2), UUID(), NOW(), NOW(), 1),
  (ROUND(RAND() * 9, 2), UUID(), NOW(), NOW(), 2),
  -- Repite la estructura para agregar más filas
  -- Insertar datos en la tabla 'ubicacion'
INSERT INTO dispositivo (latitud, longitud, external_id, createdAt, updatedAt) VALUES
  (CONCAT(ROUND(RAND() * 90, 6), '°'), CONCAT(ROUND(RAND() * 180, 6), '°'), UUID(), NOW(), NOW()),
  (CONCAT(ROUND(RAND() * 90, 6), '°'), CONCAT(ROUND(RAND() * 180, 6), '°'), UUID(), NOW() - INTERVAL 1 DAY, NOW() - INTERVAL 1 DAY),
  (CONCAT(ROUND(RAND() * 90, 6), '°'), CONCAT(ROUND(RAND() * 180, 6), '°'), UUID(), NOW() - INTERVAL 1 YEAR, NOW() - INTERVAL 1 YEAR),
  (CONCAT(ROUND(RAND() * 90, 6), '°'), CONCAT(ROUND(RAND() * 180, 6), '°'), UUID(), NOW() - INTERVAL 2 DAY, NOW() - INTERVAL 1 DAY),
  (CONCAT(ROUND(RAND() * 90, 6), '°'), CONCAT(ROUND(RAND() * 180, 6), '°'), UUID(), NOW() - INTERVAL 3 DAY, NOW() - INTERVAL 1 DAY),
  (CONCAT(ROUND(RAND() * 90, 6), '°'), CONCAT(ROUND(RAND() * 180, 6), '°'), UUID(), NOW() - INTERVAL 4 DAY, NOW() - INTERVAL 1 DAY);
  -- Repite la estructura para agregar más filas

-- Insertar datos en la tabla 'medicion'
INSERT INTO medicion (indice_uv, external_id, createdAt, updatedAt, id_dispositivo) VALUES
  (ROUND(RAND() * 9, 2), UUID(), NOW(), NOW(), 1),
  (ROUND(RAND() * 9, 2), UUID(), NOW() - INTERVAL 1 DAY, NOW() - INTERVAL 1 DAY, 2),
  (ROUND(RAND() * 9, 2), UUID(), NOW() - INTERVAL 1 YEAR, NOW() - INTERVAL 1 YEAR, 3),
  (ROUND(RAND() * 9, 2), UUID(), NOW() - INTERVAL 2 DAY, NOW() - INTERVAL 1 YEAR, 3),
  (ROUND(RAND() * 9, 2), UUID(), NOW() - INTERVAL 3 DAY, NOW() - INTERVAL 1 YEAR, 3),
  (ROUND(RAND() * 9, 2), UUID(), NOW() - INTERVAL 4 DAY, NOW() - INTERVAL 1 YEAR, 3);
  -- Repite la estructura para agregar más filas

*/